# Contacts

It eases maintenance of database contacts (structure defined in eztools and database).
<p>Key functions

 + List contact info, such as name, ID, address, group etc.
 + Import and export the info.
 + Parse locations of the addresses of the contacts and,
     - find out who is within a distance to one location
     - generate maps of the locations
</p>

# command line parameters

 - -h, --help, --version: show version and help info
 - -log {file name}: log to the file
 - -v, -vv, -vvv: enable verbose logging
 - -imp {file name}: import from a file, better to be used with -enc, if not UTF-8<BR>
It must contain header and content, with fields separated by commas.<BR>
Key fields must be included, to match a line to an existing record.
 - -exp {file name}: export to a file, better to be used with -enc, if not UTF-8<BR>
If the file already exists, it must contain at least field list as the first line.
Only listed fields are exported.<BR>
With no fields above given, all fields are exported.<BR>
Fields separated by commas.
 - -enc {encoding}: encoding of the import file. {encoding} may be
   - gbk
 - -id: ID to use for some commands
 - -i2: other ID to use for some commands
 - -addr: address to use for some commands
 - -file {(part of) file name}: file to use for some commands. For staticmapanony and staticmapnames, it is a file to be saved to, which defaults to under current directory. "map-anony-" and "map-named-" are default for these two commands and are followed by province name and map name, with png as extension.
 - -ext {extension name}: extension to use, usually to use together with -file.For staticmapanony and staticmapnames, it defaults to png.
 - -cmd {command}: command, as below, to run in silent mode
   - adduser/useradd: add a contact
   - usermod: modify a contact. -id may be used together.
   - deluser/userdel: delete a contact. -id to be used together, to be silent.
   - groupmems: assign a team leader. -id for team and -i2 for contact, to be used together, to be silent.
   - groupmod: rename a team. -id may be used together.
   - delgroup/groupdel: delete a team. -id to be used together, to be silent.
   - parsehomes/homesparse: parse contacts' addresse. The first successful result stops checking for one contact among map configurations. Map configurations are used simultaneously among contacts.
   - checkriskdist: check contacts' distance to an address. -addr to be used together, to be silent. The first successful result stops checking among map configurations. Map configurations are used simultaneously among contacts.
   - staticmapanony: generate static map by province, without user names. -file and -ext to be used optionally. Maps by all available configurations are used.
   - staticmapnames: generate static map by province, with user names. -file and -ext to be used optionally. Maps by all available configurations are used.

# config file

EZTools' config file is needed. Refer to [readme and sample file of the project](https://gitee.com/bon-ami/eztools/v3).

# database

Database is specified in [config file](#config-file) and needs to consist following fields in tables,

## table chore

The name "chore" is specified in [config file](#config-file)

- Table names, mandatory
  - [TblContacts](#table-contacts). "contacts" as in [sample](#sample-contacts).
  - [TblTeam](#table-team). "team" as in [sample](#sample-team).
- <a name="fld-names"></a>Field names, mandatory, for TblContacts, if not specified otherwise
  - FldLeader. for TblTeam. "leader" as in [sample](#sample-team).
  - FldName. "name" as in [sample](#sample-contacts).
  - FldNick. "nick" as in [sample](#sample-contacts).
  - FldTeam. "team" as in [sample](#sample-contacts).
  - FldNumber. "number" as in [sample](#sample-contacts).
  - FldAddr. if map is defined in eztools. "addr" as in [sample](#sample-contacts).
  - FldLatitude. if map is defined in eztools. "latitude" as in [sample](#sample-contacts).
  - FldLongitude. if map is defined in eztools. "longitude" as in [sample](#sample-contacts).
  - FldProvince. if map is defined in eztools. "province" as in [sample](#sample-contacts).
- record ID's for [auto update](https://github.com/sanbornm/go-selfupdate/selfupdate), optional.
  - contactsApp: app name on update server
  - contactsDir: dir name on update server
  - contactsUrl: URL of update server
  - contactsUrlDev: URL of update server for tests. A version "dev" or empty is a test version.

```
INSERT INTO TABLE chore (id, str) VALUES ("TblContacts", "contacts");
INSERT INTO TABLE chore (id, str) VALUES ("TblTeam", "team");
INSERT INTO TABLE chore (id, str) VALUES ("FldLeader", "leader");
INSERT INTO TABLE chore (id, str) VALUES ("FldName", "name");
INSERT INTO TABLE chore (id, str) VALUES ("FldNick", "nick");
INSERT INTO TABLE chore (id, str) VALUES ("FldTeam", "team");
INSERT INTO TABLE chore (id, str) VALUES ("FldNumber", "number");
INSERT INTO TABLE chore (id, str) VALUES ("FldAddr", "addr");
INSERT INTO TABLE chore (id, str) VALUES ("FldLatitude", "latitude");
INSERT INTO TABLE chore (id, str) VALUES ("FldLongitude", "longitude");
INSERT INTO TABLE chore (id, str) VALUES ("FldProvince", "province");
INSERT INTO TABLE chore (id, str) VALUES ("contactsApp", "contacts");
INSERT INTO TABLE chore (id, str) VALUES ("contactsDir", "public/");
INSERT INTO TABLE chore (id, str) VALUES ("contactsUrl", "http://1.1.1.1:8080/");
INSERT INTO TABLE chore (id, str) VALUES ("contactsUrlDev", "http://1.1.1.1:8081/");
```

## table contacts

Field ID whose name same as in [table chore](#table-chore) should be auto generated.<BR>
Fields must exist with names specified in [table chore](#fld-names). Order is flexible.<BR>
Other fields are optional and may scatter among mandatory fields.

| field | type | configured in chore |
| ----- | ---- | ------------------- |
| id | int | the primary key |
| name | string | FldName |
| nick | string | FldNick |
| number | string | FldNumber |
| team | int | FldTeam |

<a name="sample-contacts" />

```
CREATE TABLE contacts (id TINYINT AUTOINCREMENT, number TINYTEXT, name TINYTEXT NOT NULL, team TINYINT, ext TINYTEXT, phone TINYTEXT, mail TINYTEXT NOT NULL, ldap TINYTEXT, uid TINYTEXT, latitude NUMERIC(8,6), longitude NUMERIC(9,6), province TINYTEXT, PRIMARY KEY(id)) CHARSET=utf8;
```

## table team

Field ID whose name same as in [table chore](#table-chore) should be auto generated.<BR>
Field str whose name same as in [table chore](#table-chore) must exist.<BR>
Field leader whose name set in [table chore](#table-chore) must exist.<BR>

| field | type | configured in chore |
| ----- | ---- | ------------------- |
| id | int | the primary key |
| str | string | the second field |
| leader | int | FldLeader |

<a name="sample-team" />

```
CREATE TABLE team (id TINYINT AUTOINCREMENT, str TINYTEXT, leader TINYINT, PRIMARY KEY(id));
```

# operations

0. Exit
1. Add a contact
2. Modify a contact
3. Delete a contact
4. Assign a team leader
5. Rename a team
6. Delete a team
7. \*Parse contacts' addresses
8. \*Check contacts' distance to an address

"*" if map is defined in eztools
[![screenshot](https://ezproject.sourceforge.io/sc_contacts.png)](https://ezproject.sourceforge.io/sc_contacts.png)
