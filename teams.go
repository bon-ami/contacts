package main

import (
	"gitee.com/bon-ami/eztools/v4"
)

func ListTeams(db *eztools.Dbs) error {
	teamArr, err := db.Search(tblTeam, "",
		[]string{fldId, fldStr, fldLeader}, "")
	if err != nil {
		return err
	}
	var (
		contactArr [][]string
		leader     string
	)
	for _, team := range teamArr {
		contactArr, err = db.Search(tblContacts,
			fldId+"="+team[2], []string{fldsStrs[fldsIndName]}, "")
		if err != nil {
			leader = "None"
		} else {
			if len(contactArr) == 0 {
				leader = "Invalid"
			} else {
				leader = contactArr[0][0]
			}
		}
		eztools.ShowStrln(team[0] + ": " + team[1] + " leader: " + leader)
	}
	return nil
}

// ModifyTeam modify a team's name by ID
func ModifyTeam(db *eztools.Dbs, id string) error {
	nameOld, err := db.GetPairStr(tblTeam, id)
	if err != nil {
		return err
	}
	nameNew := eztools.PromptStr("New team name([Enter]=" + nameOld + ")")
	if nameNew == nameOld || len(nameNew) < 1 {
		return eztools.ErrInvalidInput
	}
	return db.UpdatePairWtParams(tblTeam, id, nameNew)
}

// DeleteTeam delete a team by ID
func DeleteTeam(db *eztools.Dbs, id string) error {
	return db.DeleteWtID(tblTeam, id)
}
